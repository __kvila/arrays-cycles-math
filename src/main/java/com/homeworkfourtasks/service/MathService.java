package main.java.com.homeworkfourtasks.service;

import java.util.Scanner;

public class MathService {

    private static final String MSG_RESULT = "Expression result: ";
    private static final String MSG_SPEED_DISTANCE = "Distance: ";
    private static final String MSG_DEGREES_OR_RADIANS = "Did you enter a value in degrees or radians?\nDegrees - enter 1\nRadians - enter 0";
    private static final String MSG_DEGREES = "In degrees: ";
    private static final String MSG_RADIANS = "In radians: ";
    private static final String MSG_INVALID_COMMAND = "Invalid command! Try again.";

    private static final double g = 9.81;

    Scanner scanner;

    public MathService(Scanner scanner){
        this.scanner = scanner;
    }

    public int runMath(){
        execTaskOne(45, 100);
        execTaskTwo(100, 85, 10, 3);
        System.out.println(execTaskThree(2, 3));
        System.out.println(execTaskFour(5.6));
        return 1;
    }

    public String execTaskOne(int a, int v){
        System.out.println(MSG_DEGREES_OR_RADIANS);
            String userResponse = scanner.nextLine();
            if (userResponse.equals("1")) {
                return MSG_DEGREES + ((Math.pow(v, 2) * Math.toRadians(Math.sin(2 * a))) / g);
            } else if (userResponse.equals("0")) {
                return MSG_RADIANS + ((Math.pow(v, 2) * Math.toRadians(Math.sin(2 * a))) / g);
            } else {
                return MSG_INVALID_COMMAND;
            }
    }

    public int execTaskTwo(int v1, int v2, int s, int t){
        for (int i = 0; i < t; i++) {
            s += v1 + v2;
        }

        return s;
    }

    public int execTaskThree(double x, double y){
        double x1, x2, x3, y1, y2, y3, compOne = 0, compTwo = 0, compThree = 0;

        if(x == 0 && (y > 0 || y < -1)){
            System.out.println("Doesn't lie!");
            return 0;
        }

        if((x >=0 && y >=0) || x >=0 && y <=0) {
            x1 = 0;
            x2 = 2;
            x3 = 0;
            y1 = 0;
            y2 = 2;
            y3 = -1;
            compOne = (x1 - x) * (y2 - y1) - (x2 - x1) * (y1 - y);
            compTwo = (x2 - x) * (y3 - y2) - (x3 - x2) * (y2 - y);
            compThree = (x3 - x) * (y1 - y3) - (x1 - x3) * (y3 - y);
        } else if((x <= 0 && y >=0) || (x <= 0 && y <= 0)){
            x1 = -2;
            x2 = 0;
            x3 = 0;
            y1 = 2;
            y2 = 0;
            y3 = -1;
            compOne = (x1 - x) * (y2 - y1) - (x2 - x1) * (y1 - y);
            compTwo = (x2 - x) * (y3 - y2) - (x3 - x2) * (y2 - y);
            compThree = (x3 - x) * (y1 - y3) - (x1 - x3) * (y3 - y);
        }

        if((compOne > 0 && compTwo > 0 && compThree > 0) || (compOne < 0 && compTwo < 0 && compThree < 0)){
            System.out.println("Lies!");
            return 1;
        } else
            if(compOne == 0 || compTwo == 0 || compThree == 0){
                System.out.println("Lies!");
                return 1;
            } else {
                System.out.println("Doesn't lie!");
                return 0;
            }
    }

    public double execTaskFour(double x){
        double z;
        z = ((6 * Math.log(Math.sqrt(Math.exp(x + 1) + 2 * Math.exp(x) * Math.cos(x)))) / (Math.log(x - Math.exp(x + 1) * Math.sin(x)))) + (Math.abs(Math.cos(x) / Math.exp(Math.sin(x))));
        System.out.print(MSG_RESULT);
        return z;
    }
}
