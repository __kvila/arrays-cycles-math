package main.java.com.homeworkfourtasks.service;

import java.util.Random;
import java.util.Scanner;

public class RandomService {
    private static final String MSG_TASK_ONE = "Task one: ";
    private static final String MSG_TASK_TWO = "Task two: ";
    private static final String MSG_TASK_THREE = "Task three: ";
    private static final String MSG_TASK_FOUR = "Task four: ";
    private static final String MSG_TASK_FIVE = "Task five: ";
    private static final String MSG_TASK_SIX = "Task six: ";

    private Scanner scanner;
    private Random random;

    public RandomService(){
        this.scanner = new Scanner(System.in);
        this.random = new Random();
    }

    public void runMethods(){
        randomNumberOne();
        randomNumberTwo();
        randomNumberThree();
        randomNumberFour();
        randomNumberFive();
        randomNumberSix();
    }

    public void randomNumberSix(){
        System.out.println(MSG_TASK_SIX);
        int rNum = random.nextInt(13) + 3;
        for (int i = 0; i < rNum; i++) {
            System.out.println(35 - random.nextInt(46));
        }
    }

    public void randomNumberFive(){
        System.out.println(MSG_TASK_FIVE);
        int rNum;
        for (int i = 0; i < 10; i++) {
            rNum = random.nextInt(21) - 10;
            System.out.println(rNum);
        }
    }

    public void randomNumberFour(){
        System.out.println(MSG_TASK_FOUR);
        int rNum;
        for (int i = 0; i < 10; i++) {
            rNum = random.nextInt(31) + 20;
            System.out.println(rNum);
        }
    }

    public void randomNumberThree(){
        System.out.println(MSG_TASK_THREE);
        for (int i = 0; i < 10; i++) {
            System.out.println(random.nextInt(11));
        }
    }

    public void randomNumberTwo(){
        System.out.println(MSG_TASK_TWO);
        for (int i = 0; i < 10; i++) {
            System.out.println(random.nextInt());
        }
    }

    public void randomNumberOne(){
        System.out.println(MSG_TASK_ONE);
        System.out.println(random.nextInt());
    }
}
