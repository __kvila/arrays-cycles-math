package main.java.com.homeworkfourtasks.service;

public class ArraysService {

    private static final String MSG_FIGURE_ONE = "Figure number one: ";
    private static final String MSG_FIGURE_TWO = "Figure number two: ";
    private static final String MSG_FIGURE_THREE = "Figure number three: ";
    private static final String MSG_FIGURE_FOUR = "Figure number four: ";
    private static final String MSG_FIGURE_FIVE = "Figure number five: ";
    private static final String MSG_FIGURE_SIX = "Figure number six: ";
    private static final String MSG_FIGURE_SEVEN = "Figure number seven: ";
    private static final String MSG_FIGURE_EIGHT = "Figure number eight: ";
    private static final String MSG_FIGURE_NINE = "Figure number nine: ";

    public ArraysService(){ }

    char [][] array = new char [7][7];

    public void runArraysMethods(){
        clearArray();
        setFigureOne();
        setFigureTwo();
        setFigureThree();
        setFigureFour();
        setFigureFive();
        setFigureSix();
        setFigureSeven();
        setFigureEight();
        setFigureNine();
    }

    private void setFigureOne(){
        System.out.println(MSG_FIGURE_ONE);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[i][j] = '*';
            }
        }
        showArray();
        clearArray();
    }

    private void setFigureTwo(){
        System.out.println(MSG_FIGURE_TWO);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if(i == 0 || i == 6 || j == 0 || j == 6) {
                    array[i][j] = '*';
                }
            }
        }
        showArray();
        clearArray();
    }

    private void setFigureThree(){
        System.out.println(MSG_FIGURE_THREE);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if(i == 0 || j == 0 || (i + j == 6))
                    array[i][j] = '*';
            }
        }
        showArray();
        clearArray();
    }

    private void setFigureFour(){
        System.out.println(MSG_FIGURE_FOUR);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if(i == 6 || j == 0 || i == j)
                    array[i][j] = '*';
            }
        }
        showArray();
        clearArray();
    }

    private void setFigureFive(){
        System.out.println(MSG_FIGURE_FIVE);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if(i == 6 || j == 6 || (i + j == 6))
                    array[i][j] = '*';
            }
        }
        showArray();
        clearArray();
    }

    private void setFigureSix(){
        System.out.println(MSG_FIGURE_SIX);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if(i == 0 || j == 6 || i == j)
                    array[i][j] = '*';
            }
        }
        showArray();
        clearArray();
    }

    private void setFigureSeven(){
        System.out.println(MSG_FIGURE_SEVEN);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if(i + j == 6 || i == j)
                    array[i][j] = '*';
            }
        }
        showArray();
        clearArray();
    }

    private void setFigureEight(){
        System.out.println(MSG_FIGURE_EIGHT);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if(i == 0 || ((i == j) && i < 4) || ((i + j == 6) && i < 4))
                    array[i][j] = '*';
            }
        }
        showArray();
        clearArray();
    }

    private void setFigureNine(){
        System.out.println(MSG_FIGURE_NINE);
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if(i == 6 || ((i == j) && i > 2) || ((i + j == 6) && i > 2))
                    array[i][j] = '*';
            }
        }
        showArray();
        clearArray();
    }

    private void clearArray(){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[i][j] = ' ';
            }
        }
    }

    private void showArray(){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
