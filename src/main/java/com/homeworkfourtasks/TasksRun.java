package main.java.com.homeworkfourtasks;

import main.java.com.homeworkfourtasks.service.ArraysService;
import main.java.com.homeworkfourtasks.service.MathService;
import main.java.com.homeworkfourtasks.service.RandomService;

import java.util.Random;
import java.util.Scanner;

public class TasksRun {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //new RandomService().runMethods();

        //new ArraysService().runArraysMethods();

        new MathService(scanner).runMath();
    }
}
