package main.test.com.homeworkfourtasks.service;

import main.java.com.homeworkfourtasks.service.MathService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Scanner;

public class MathServiceTest {
    private final Scanner scanner = Mockito.mock(Scanner.class);
    private final MathService cut = new MathService(scanner);

    static Arguments[] execTaskOneTestArgs() {
        return new Arguments[]{
                Arguments.arguments(30, 100, "1", "In degrees: -5.42298566083852"),
                Arguments.arguments(30, 100, "0", "In radians: -5.42298566083852"),
                Arguments.arguments(30, 100, "2", "Invalid command! Try again.")
        };
    }

    @ParameterizedTest
    @MethodSource("execTaskOneTestArgs")
    void execTaskOneTest(int a, int v, String expScanner, String expected) {
        Mockito.when(scanner.nextLine()).thenReturn(expScanner);

        String actual = cut.execTaskOne(a, v);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] execTaskTwoTestArgs() {
        return new Arguments[]{
                Arguments.arguments(80, 100, 75, 3, 615),
                Arguments.arguments(100, 100, 0, 1, 200),
                Arguments.arguments(0, 0, 0, 3, 0)
        };
    }

    @ParameterizedTest
    @MethodSource("execTaskTwoTestArgs")
    void execTaskTwoTest(int v1, int v2, int s, int t, int expected) {

        int actual = cut.execTaskTwo(v1, v2, s, t);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] execTaskThreeTestArgs() {
        return new Arguments[]{
                Arguments.arguments(2, 2, 1),
                Arguments.arguments(2, 3, 0),
                Arguments.arguments(0, -1.1, 0),
                Arguments.arguments(0, -0.5, 1)
        };
    }

    @ParameterizedTest
    @MethodSource("execTaskThreeTestArgs")
    void execTaskThreeTest(double x, double y, int expected) {

        int actual = cut.execTaskThree(x, y);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] execTaskFourTestArgs() {
        return new Arguments[]{
                Arguments.arguments(3.16, 6.80203350189627),
                Arguments.arguments(5.66, 4.942000209161543),
                Arguments.arguments(0, 1)
        };
    }

    @ParameterizedTest
    @MethodSource("execTaskFourTestArgs")
    void execTaskFourTest(double x, double expected) {

        double actual = cut.execTaskFour(x);

        Assertions.assertEquals(expected, actual);
    }

    static Arguments[] runMathTestArgs() {
        return new Arguments[]{
                Arguments.arguments(30, 100, "1", "In degrees: -5.42298566083852",
                        80, 100, 75, 3, 615,
                        2, 2, 1,
                        3.16, 6.80203350189627)
        };
    }

    @ParameterizedTest
    @MethodSource("runMathTestArgs")
    void runMathTest(int a, int v, String expScanner, String expected1, int v1, int v2, int s, int t, int expected2,
                      double x, double y, int expected3, double xx, double expected4) {

        Mockito.when(scanner.nextLine()).thenReturn(expScanner);

        String actual1 = cut.execTaskOne(a, v);
        int actual2 = cut.execTaskTwo(v1, v2, s, t);
        int actual3 = cut.execTaskThree(x, y);
        double actual4 = cut.execTaskFour(xx);

        Assertions.assertEquals(expected1, actual1);
        Assertions.assertEquals(expected2, actual2);
        Assertions.assertEquals(expected3, actual3);
        Assertions.assertEquals(expected4, actual4);
    }
}
